var request = require('request');
var apiURL = {
  server: "http://localhost:" + process.env.PORT
};
if (process.env.API_URL) {
  apiURL.server = process.env.API_URL;
}

var getFilesAndReturnView = function(res) {
    request({
        url: apiURL.server + "/api/files",
        method: 'GET',
        json: {},
    }, function callback(err, code, body) {
        res.render('fribox', {files: body});
    });
}


module.exports.fribox = function(req, res, next) {
    getFilesAndReturnView(res);
}

module.exports.upload = function(req, res, next) {
    if (!req.files)
        //No files uploaded
        res.redirect('/fribox');
    
    var file = req.files.file;
    
    var formData = {
        data: {
            value:  file.data,
            options: {
                filename: file.name,
                contentType: file.mimetype
            }
        }
    };
    
    request.post({url:apiURL.server + "/api/file", formData: formData}, function callback(err, httpResponse, body) {
        res.redirect('/fribox');
    });
}

module.exports.delete = function(req, res, next) {
    var filename = req.body.filename;
    
    request.delete({
        url:apiURL.server + "/api/file", 
        formData: { filename: filename }
        
    }, function callback(err, status, body) {
        res.redirect('/fribox');
    });
}