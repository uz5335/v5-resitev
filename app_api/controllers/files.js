var mongoose = require('mongoose');
var File = mongoose.model('File');

var returnJson = function(res, status, content) {
  res.status(status);
  res.json(content);
};

module.exports.getFile = function(req, res) {
    File.findOne({name: req.params.filename})
        .exec(function(err, file) {
            //TODO: perform error handling ...
            var header = {};
            if (req.params.type == "download") {
                //Download
                header = {
                    'content-type': 'application/octet-stream',
                    'content-disposition': 'attachment; filename="' + file.name + '"'
                };
            } else {
                //View
                header = {
                    'content-type': file.mimeType
                };
            }
            
            res.writeHead(200, header);
            res.end(file.data);
        });
}

module.exports.listFiles = function(req, res) {
    File.find()
        .select('name size')
        .exec(function(err, results) {
            returnJson(res, 200, results);
        });
}

module.exports.deleteFile = function(req, res) {
    var filename = req.body.filename;
    
    //Remove file from DB
    File.remove({name: filename})
        .exec(function(err) {
            var deleted = (err ? false : true);
            returnJson(res, 200, {"deleted": deleted});
        });
}

module.exports.saveFile = function(req, res) {
    var file = req.files.data;
    console.log(req.files);
    
    //Limit to max 100 files in the system
    //Limit to max 4MB per file
    File.find()
        .select('name')
        .exec(function(err, results) {
            if (results.length >= 100 || file.data.byteLength > (4 * 1024 * 1024)) {
                //Ignore
                returnJson(res, 200, {"saved": false});
            } else {
                //Save file to DB
                var fileDB = new File({
                    _id: new mongoose.Types.ObjectId(),
                    name: file.name,
                    size: file.data.byteLength,
                    mimeType: file.mimetype,
                    data: file.data
                });
                
                fileDB.save(function(err) {
                    var saved = (err ? false : true);
                    returnJson(res, 200, {"saved": saved});
                });
            }
        });
    
    
};